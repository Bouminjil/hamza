var express = require('express');
var router = express.Router();
var Product = require('../models/product');
/* GET home page. */
router.get('/store', function(req, res, next) {
  Product.find(function(err, docs) {
    var productChunks = [];
    var chunkSize = 3;
    for (var i =0; i < docs.length; i += chunkSize) {
        productChunks.push(docs.slice(i, i + chunkSize));
      }
    res.render('shop/index', { title: 'E.P.I', products: productChunks });
  });
});

module.exports = router;

router.get('/home', function(req, res, next) {
  
    res.render('Home/home');
  });

  router.get('/contact', function(req, res, next) {
  
    res.render('contact');
  });